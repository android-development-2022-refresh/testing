package stanchfield.scott.composetest

import kotlinx.coroutines.flow.flow

class MockRepository: MovieRepository() {
    var getMoviesCalled = false
    var saveMovieCalled = false
    override fun getMovies() = flow {
        getMoviesCalled = true
        emit(listOf(Movie("m1", "Die Hard"), Movie("m2", "Get Smart")))
    }

    override fun saveMovie(movie: Movie) {
        saveMovieCalled = true
    }
}