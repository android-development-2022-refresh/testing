package stanchfield.scott.composetest

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDAO {
    @Query("SELECT * FROM Movie ORDER BY TITLE")
    fun getMovies(): Flow<List<Movie>>

    @Update
    fun saveMovie(movie: Movie)
}